## Iotlogi
App para visualizacion y analisis de datos en tiempo real del proyecto _trazabilidad de alimentos en cadenas de frío_

Presentación del proyecto -> [Ver Slides](https://juliocesar.io/talks/2015-01-20-trazabilidad-en-cadenas-de-frio/)

## Novedades v0.5 (API)
- [x] Refactorización del API -> Cambio de Nodejs a Python
- [x] Cambio de motor db a Postgres
- [x] Implementación de token's y gestión de usuarios
- [x] Bootstrapping del landing
- [x] Configurarción para servidor de producción (Nginx, Gunicorn)
- [x] Servidor de producción (Testing)
    - Domnino: iotlogi.com
    - 512MB Ram
    - 20GB SSD Disk
    - Django on Ubuntu 14.04
- [ ] Manegar el POST del cliente Android


## Dependencias

- Express
- Socket
- Jade
- Redis


## Correr el demo

Instalar dependencias con npm

```
$ npm install
```

Descargar e instalar redis


```
$ wget http://download.redis.io/releases/redis-3.0.4.tar.gz
$ tar xzf redis-3.0.4.tar.gz
$ cd redis-3.0.4
$ make

```

Iniciar servidor de redis

```
$ src/redis-server
```


Ejecutar la app

```
$ node app.js
```

Visualizar coordenadas de prueba en el mapa

```
$ node demo/genCoordenadas.js
```

Visitar [http://localhost:8070](http://localhost:8070)
