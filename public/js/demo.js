var header_height = 60;
var footer_height = 15;
var map_height_min = 350;

google.maps.visualRefresh = true;

function resize() {
	var non_map_content_height = header_height + footer_height;
	var map_height = map_height_min;
	var difference = $(window).height( ) - non_map_content_height;
	if ( difference > map_height )
	  map_height = difference;
	$("#map").css( 'height', map_height );
}


var centerOn = -1;

$(window).resize(function() {
  resize();
});

$(document).ready( function(){
  resize();

var opts = {
  center: new google.maps.LatLng(10.3910466, -75.5144455),
  zoom: 17,
  mapTypeId: google.maps.MapTypeId.ROADMAP
};

// Crear el mapa
map = new google.maps.Map( document.getElementById("map"), opts );

socket.event.subs.begin = function(data){
  $('#centerList tr').remove();

  $('input:radio[name=centerOn]').last().change(function(){
	centerOn = -1;
  });
}

});

// function demo(){
//   socket.emit('sub', 'Demo');
//   socket.emit('subs');
//   map.setZoom(20);
// }
